﻿using System;
using BankLibrary;

namespace BankApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Bank<Account> bank = new Bank<Account>("ЮнитБанк");
            while (true)
            {
                ConsoleColor color = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("1. Открыть счёт\n2. Вывести средства\n3. Добавить на счёт\n4. Закрыть счёт\n5. Пропустить день\n6. Выйти из программы");
                Console.WriteLine("Введите номер пункта:");
                Console.ForegroundColor = color;
                switch (Console.ReadLine())
                {
                    case "1":
                        OpenAccount(bank);
                        break;
                    case "2":
                        Withdraw(bank);
                        break;
                    case "3":
                        Put(bank);
                        break;
                    case "4":
                        CloseAccount(bank);
                        break;
                    case "5":
                        break;
                    case "6":
                        return;
                    default:
                        color = Console.ForegroundColor;
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Ошибка! Повторите 1-6!");
                        Console.ForegroundColor = color;
                        break;
                }
                bank.CalculatePrecentage();

            }
        }

        private static void OpenAccount(Bank<Account> bank)
        {
            Console.WriteLine("Укажите сумму для создания счёта:");
            decimal sum = Convert.ToDecimal(Console.ReadLine());
            Console.WriteLine("Выберите тип счёта:\n1. До востребования\n2. Депозит");
            AccountType accountType;
            switch (Console.ReadLine())
            {

                case "2":
                    accountType = AccountType.Deposit;
                    break;
                default:
                    accountType = AccountType.Ordinary;
                    break;
            }
            bank.Open(accountType, sum,
                AddSumHandler, WithdrawSumHandler,
                (o, e) => Console.WriteLine(e.Message),
                CloseAccountHandler, OpenAccountHandler);
        }
        private static void Withdraw(Bank<Account> bank)
        {
            Console.WriteLine("Укажите сумму для вывода со счёта:");

            decimal sum = Convert.ToDecimal(Console.ReadLine());
            Console.WriteLine("Введите Id счёта:");
            int id = int.Parse(Console.ReadLine());

            bank.Withdraw(sum, id);
        }

        private static void Put(Bank<Account> bank)
        {
            Console.WriteLine("Укажиет сумму, чтобы положить на счёт;");
            decimal sum = Convert.ToDecimal(Console.ReadLine());
            Console.WriteLine("Введите Id счёта:");
            int id = int.Parse(Console.ReadLine());
            bank.Put(sum, id);
        }

        private static void CloseAccount(Bank<Account> bank)
        {
            Console.WriteLine("Введите id счёта, который надо закрыть:");
            int id = int.Parse(Console.ReadLine());
            bank.Close(id);
        }


        private static void OpenAccountHandler(object sender, AccountEventArgs e)
        {
            Console.WriteLine(e.Message);
        }

        private static void AddSumHandler(object sender, AccountEventArgs e)
        {
            Console.WriteLine(e.Message);
        }

        private static void WithdrawSumHandler(object sender, AccountEventArgs e)
        {
            Console.WriteLine(e.Message);
            if (e.Sum>0)
                Console.WriteLine("Идём тратить деньги");
        }

        private static void CloseAccountHandler(object sender, AccountEventArgs e)
        {
            Console.WriteLine(e.Message);
        }
    }
}
